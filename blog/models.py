from django.db import models
from django.utils import timezone


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Comments(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    text = models.TextField(blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    post = models.ForeignKey('Post', on_delete=models.CASCADE, null=True, blank=True)

    def comment(self):
        self.create_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title