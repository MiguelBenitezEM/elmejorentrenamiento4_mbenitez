from django import forms
from django.contrib import auth
from .models import Post, Comments

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text')

class UserForm(auth.forms.UserCreationForm):
    
    class Meta:
        model = auth.models.User
        fields = ('username', 'email', 'password1', 'password2')

class LoginForm(auth.forms.AuthenticationForm):

    class Meta:
        model = auth.models.User
        fields = ('email', 'password')

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = ('title', 'text')