from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from blog.models import Post, Comments
from .forms import PostForm, UserForm, LoginForm, CommentForm
from django.contrib import auth
from django.http import JsonResponse
from plyer import notification

# Mostrar el feed, la lista de posts
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    notification.notify(title="notificacion", message="notificacion")
    if len(posts) < 1:
        return render(request, 'blog/null.html', {})
    else:
        return render(request, 'blog/post_list.html', {'posts': posts})

# Detalles de cada post
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    comments = Comments.objects.filter(post=pk).order_by('-pk')
    if request.method == "POST" and request.is_ajax():
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.create_date = timezone.now()
            comment.post = post
            comment.save()
            comment_to_json = {'comment': {'title': comment.title, 'text': comment.text, 'create_date': comment.create_date, 'author': comment.author.username}}
            return JsonResponse(comment_to_json, status=200)
        else:
            errors = form.errors.as_json()
            return JsonResponse({"errors": errors}, status=400)
    else:
        form = CommentForm()
    return render(request, 'blog/post_detail.html', {'post': post, 'comments': comments, 'form': form})

# Crear un nuevo post
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

# Editar un post
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.user == post.author:
        if request.method == "POST":
            form = PostForm(request.POST, instance=post)
            if form.is_valid():
                post = form.save(commit=False)
                post.author = request.user
                post.published_date = timezone.now()
                post.save()
                return redirect('post_detail', pk=post.pk)
        else:
            form = PostForm(instance=post)
        return render(request, 'blog/post_edit.html', {'form': form})
    else:
        return render(request, 'blog/forbidden.html')

def post_delete(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.user == post.author:
        post.delete()
        return redirect('post_list')
    else:
        return render(request, 'blog/forbidden.html')

# Crear un nuevo usuario
def create_user(request):
    if request.method == "POST":
        form = UserForm(data=request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            if len(auth.models.User.objects.filter(email=user.email)) < 1:
                user.save()
                auth.login(request, user)
                return redirect ('post_list')
    else:
        form = UserForm()
    return render(request, 'blog/create_user.html', {'form': form})

# Iniciar sesion
def log_in(request):
    if request.method == "POST":
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                return redirect('post_list')
            
    else:
        form = auth.forms.AuthenticationForm()
    return render(request, 'blog/login.html', {'form': form})

# Cerrar sesion
def log_out(request):
    auth.logout(request)
    return redirect('post_list')

# Editar comentario
def comment_edit(request, pk):
    comment = get_object_or_404(Comments, pk=pk)
    post = comment.post
    if request.user == comment.author:
        if request.method == "POST":
            form = CommentForm(request.POST)
            if form.is_valid():
                comment.save()
                return redirect('post_detail', pk=post.pk)
        
        else:
            form = CommentForm(instance=comment)
        return render(request, 'blog/comment_edit.html', {'form': form})
    else:
        return render(request, 'blog/forbidden.html')

# Eliminar un comentario
def comment_delete(request, pk):
    comment = get_object_or_404(Comments, pk=pk)
    if request.user == comment.author:
        comment.delete()
        post = comment.post
        return redirect('post_detail', pk=post.pk)
    else:
        return render(request, 'blog/forbidden.html')