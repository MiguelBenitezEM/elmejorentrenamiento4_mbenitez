from django.urls import path
from . import views

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new/', views.post_new, name='post_new'),
    path('post/<int:pk>/edit/', views.post_edit, name='post_edit'),
    path('post_del/<int:pk>/', views.post_delete, name='post_del'),
    path('new/user/', views.create_user, name='create_user'),
    path('login/', views.log_in, name='login'),
    path('logout/', views.log_out, name='log_out'),
    path('comment/<int:pk>/edit/', views.comment_edit, name='comment_edit'),
    path('comment/<int:pk>/del/', views.comment_delete, name='comment_del'),
]